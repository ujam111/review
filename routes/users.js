var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

module.exports = router;

const crypto = require('crypto');


router.post("/sign_up", function(req,res,next){
  let body = req.body;

  crypto.randomBytes(64, function(err, buf) {
    crypto.pbkdf2(body.password, buf.toString('base64'), 100000, 64, 'sha512', async function(err, key){
      result = await models.user.create({
        name: body.userName,
        email: body.userEmail,
        password: key,
        salt: buf
      })
      
      res.redirect("/user/sign_up");
    });
  });
})